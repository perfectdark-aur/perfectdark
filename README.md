![picture](https://cdn2.steamgriddb.com/logo_thumb/7b99efbc101a6013d2c710028bca5cbf.png) 

﻿Perfect Dark Powered by the perfectdark reimplimentation engine 
This has no affiliation with Perfect Dark or the open source reimplimentation engine.

Repository for AUR package
Packages:
[pd-bin](https://aur.archlinux.org/packages/pd-bin))
[perfectdark](https://aur.archlinux.org/packages/perfectdark))

 ### Author
  * Corey Bruce
